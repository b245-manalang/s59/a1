import {Container, Nav, Navbar} from 'react-bootstrap';

import {Link, NavLink} from 'react-router-dom';
import {useContext, Fragment} from 'react';
import UserContext from '../UserContext.js';
export default function NavBar(){

  console.log(localStorage.getItem('email'));

    const {user} = useContext(UserContext);

	   return (
    <Navbar id="NavBar1" bg="light" expand="lg">
      <Container>
        <Navbar.Brand as = {Link} to = '/'></Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="ms-auto">
            <Nav.Link as = {NavLink} to = '/'>Home</Nav.Link>
           
            {/*conditional rendering*/}
            {
            user ?
             <Nav.Link as = {NavLink} to = '/logout'>Logout</Nav.Link>
              :
             <Fragment>
               <Nav.Link as = {NavLink} to = '/register'>Register</Nav.Link>
               <Nav.Link as = {NavLink} to = '/login'>Login</Nav.Link>
             </Fragment>
            }
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}
