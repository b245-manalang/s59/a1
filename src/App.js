
// import './App.css';
// import Home from './pages/Home'
// import NavBar from './components/Navbar.jsx';
// function App() {
//   return (
//     <Home/>

//   );
// }

// export default App;

import './App.css';

// import {Container} from 'react-bootstrap';
import {useState, useEffect} from 'react';
import NavBar from './components/Navbar.js';
/*import Banner from './Banner.js';
import Highlights from './Highlights.js';*/
import Home from './pages/Home.js';
import Login from './pages/Login.js';
import Register from './pages/Register.js';
import Logout from './pages/Logout.js';
import NotFound from './pages/NotFound.js'
// import CourseView from './components/CourseView.js'
//import modules from react-router-dom for the routing

//import the UserProvider
import {UserProvider} from './UserContext.js';

import { BrowserRouter as Router, Routes, Route} from 'react-router-dom';

function App() {

  const [user, setUser] = useState(null);

  useEffect(()=>{
    console.log(user);
  }, [user]);

  const unSetUser = ()=>{
    localStorage.clear();
  }

  //Storing information in a context object is done by providing the information using the correspoding "Provider" and passing information thru the prop value;
  //all infromation/data provided to the Provider component can be access later on from the context object properties
    
  return (
    <UserProvider value ={{user, setUser, unSetUser}}>
          <Router>
            <NavBar/>
            <Routes>
                <Route path = "*" element = {<NotFound/>}/>
                <Route path="/" element = {<Home/>} />
                <Route path ="/login" element = {<Login/>}/>
                <Route path = "/register" element = {<Register/>}/>
                <Route path = "/logout" element = {<Logout/>}/>
                {/*<Route path = "/course/:courseId" element = {<CourseView/>}/>*/}
                
            </Routes>
          </Router>
    </UserProvider>
  );
}

export default App;
