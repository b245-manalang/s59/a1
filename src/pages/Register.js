import {Button, Form, Row, Col} from 'react-bootstrap';
import {Fragment} from 'react';
import {Navigate, useNavigate} from 'react-router-dom';
import {user, useState, useEffect} from 'react';
import Swal from 'sweetalert2';

export default function Register(){

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [confirmPassword, setConfirmPassword] = useState('');
	const navigate = useNavigate();
	const [isActive, setIsActive] = useState(false);

	useEffect(()=> {
		if(email !== '' && password !== '' && confirmPassword !== '' && password === confirmPassword){
				setIsActive(true);
		}else{
				setIsActive(false);
			}
	
	}, [email, password, confirmPassword])

	function register(event){
		event.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/user/register`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password,
				confirmPassword: confirmPassword
			})
		})
		.then(result => result.json())
		.then(data => {
			console.log(data)

			if(data === true){
				Swal.fire({
					title: 'Registration failed!',
					icon: 'error',
					text: 'Please try again!'
				})

			}else{
				localStorage.setItem('token', data.auth);
		
				Swal.fire({
					title: 'Registration succesful!',
					icon: 'success',
					text: 'Welcome to my shop!'
				})
				navigate('/login');
			}
		})
	}
	return(
		user?
		<Navigate to = '*'/>
		:
		<Row className = "mt-5">
			<Col className = 'col-md-6 col-10 mx-auto bg-none p-3'>
				<Fragment>
				<h1 className = 'text-center'>Register</h1>
					<Form className = 'mt-5' onSubmit = {event => register(event)}>
					<Form.Group className="mb-3" controlId="formBasicEmail">
						<Form.Label>Email address</Form.Label>
						<Form.Control 
							type="email" 
							placeholder="Enter email"
							value = {email}
							onChange = {event => setEmail(event.target.value)}
							required
							/>
						<Form.Text className="text-muted">
						We'll never share your email with anyone else.
						</Form.Text>
					</Form.Group>

					<Form.Group className="mb-3" controlId="formBasicPassword">
						<Form.Label>Password</Form.Label>
						<Form.Control 
							type="password" 
							placeholder="Password"
							value = {password}
							onChange = {event => setPassword(event.target.value)}
							required
							/>
					</Form.Group>

						<Form.Group className="mb-3" controlId="formBasicConfirmPassword">
						<Form.Label>Confirm Password</Form.Label>
						<Form.Control 
							type="password" 
							placeholder="Confirm your password"
							value = {confirmPassword}
							onChange = {event => setConfirmPassword(event.target.value)}
							required
							/>
					</Form.Group>

					{
						isActive ?
						<Button variant="primary" type="submit">
						Submit
						</Button>
						:
						<Button variant="danger" type="submit" disabled>
						Submit
						</Button>
					}
					
					
					</Form> 
			</Fragment>
		</Col>
	</Row>
  );
}